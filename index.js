//[section] Document object MODEL (DOM)

//allow us to access or modify the properties of an html elemen in a webpage
// it is standard on how to get, change, add or delete HTML elements

//We will be focusing only with DOM in term of managing forms

//for selectin html elements we will be using document.querySelector / getElementById 
  //Syntax: document.querySelector("html ekement")
   
  //CSS selectors 
  //class selector(.)
  //id selector (#)
  // tag selector (html tags)
  // universal (*)
  // attribute selector ([attribute selector])

  let universalSelector  = document.querySelectorAll("*")

  console.log(universalSelector);

  let singleUniversalSelector  = document.querySelector("*")
    console.log(singleUniversalSelector)

  let classSelector = document.querySelector(".full-name")
console.log(classSelector)
  let singleClassSelector = document.querySelectorAll(".full-name")
  console.log(singleClassSelector)

  let firstName = document.querySelector("#first-name")
   console.log(firstName)

  let tagSelector = document.querySelectorAll("input")
  console.log(tagSelector)
  let tagSpan = document.querySelector("span[id]")
  console.log(tagSpan)

  //getElement
     let element = document.getElementById("fullName")


     //[section] EventListeners
     //whenever a user interacts with a webpage,
     //this action is considered as an event/
     //workinng with events is large part of creating
     //interactivity in a webpage//
     //specific function that will be triggered if the event happen

     //the function us is "addEventListener", it take two argument
     //first argument a string identifyin the event
     //second argument, function that thethe listener will trigger obnce the
     
     let txtFirstName = document.querySelector("#txt-first-name")
        console.log(txtFirstName.value)
        //adeventlister
        txtFirstName.addEventListener("keyup", ()=>{
          console.log(txtFirstName.value)
          tagSpan.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
        })

        let txtLastName = document.querySelector("#txt-last-name");

        txtLastName.addEventListener("keyup", ()=>{
    
          tagSpan.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
        })

        let colors = document.querySelector("#text-color")
        console.log(colors)
        
        colors.addEventListener("change", (sorryIamLate)=>{
           tagSpan.style.color = sorryIamLate.target.value
        })
        

      
     